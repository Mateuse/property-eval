# Property Evaluation

### Only compatible with python 3

### To run virtual enviornment
1. `virtualenv -p python3 envname`
2. `source envname/bin/activate`

### Run all tests
1. `cd src`
2. `python -m unittest discover`