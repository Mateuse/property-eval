import sys
sys.path.append('main')

import unittest
from main.server import app
import json

class test_flask(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        self.test_post =  {
                "price": 100000,
                "yearly_gross": 10000,
                "utilities": "True",
                "downpayment": 20,
                "rate": 5,
                "years": 30
        }

    def test_main_data(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data.decode(), 'Property Evaluation \nUnder Construction')

    def test_calculations_all(self):
        result = self.app.post('/api/calculations/all', data=json.dumps(self.test_post))
        data = json.loads(result.data.decode())
        
        self.assertEqual(result.status_code, 200)
        self.assertEqual(data['downpayment'], 20000)
        self.assertEqual(data['cashflow'], 4846.48)
        self.assertEqual(data['monthly'], 429.46)
        self.assertEqual(data['noi'], 10000)
        self.assertEqual(data['one'], 1.94)

    def test_test_page(self):
        result = self.app.get('/test')
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data.decode(), "This is a test page")

if __name__ == '__main__':
    unittest.main()

