import unittest
from main.python.building import Building
from main.python.calculations import Calculations

class test_calculations(unittest.TestCase):
    def setUp(self):
        self.building = Building(100000, 10000, True)
        self.calculations = Calculations(20, 5, 30, self.building)
    
    def test_monthly_payment(self):
        self.assertEqual(429.46, self.calculations.monthly_payment())

    def test_one_percent(self):
        self.assertEqual(1.94, self.calculations.one_percent())

    def test_noi(self):
        self.assertEqual(10000, self.calculations.noi())
    
    def test_cashflow(self):
        self.assertEqual(4846.48, self.calculations.cashflow())

if __name__ == '__main__':
    unittest.main()


# To run 'python -m unittest test.python.test_calculations'