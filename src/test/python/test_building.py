import unittest
from main.python.building import Building

class test_building(unittest.TestCase):
    def setUp(self):
        self.building = Building(100000, 10000, "True")
        self.building2 = Building(100000, 12000, "False")

    def test_monthly_gross(self):
        self.assertEqual(1000, self.building2.monthly_gross())

    def test_set_tax(self):
        self.building.set_tax(10)
        self.assertEqual(10, self.building.tax)

    def test_set_insurance(self):
        self.building.set_insurance(10)
        self.assertEqual(120, self.building.insurance)

if __name__ == '__main__':
    unittest.main()


# To run 'python -m unittest test.python.test_building'