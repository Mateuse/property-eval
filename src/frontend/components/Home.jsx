import React from 'react';
import axios from 'axios';


class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            price: '',
            yearly_gross: '',
            utilities: '',
            downpayment: '',
            rate: '',
            years: ''
        };


        this.handleChange = this.handleChange.bind(this);
        this.propertyValue = this.propertyValue.bind(this);
    }
    
    handleChange(event){
        const target = event.target;
        const name = target.name;

        this.setState({[name]: target.value});
    }

    propertyValue(event){
        const property = {
            "price": parseInt(this.state.price),
            "yearly_gross": parseInt(this.state.yearly_gross),
            "utilities": parseInt(this.state.utilities),
            "downpayment": parseInt(this.state.downpayment),
            "rate": parseInt(this.state.rate),
            "years": parseInt(this.state.years)
        }
    
        axios.post("http://localhost:3001/api/calculations/all", property)
        .then((res) => {
            console.log(res.data)
        }).catch((err) => {
            console.log(err);
        });

        this.setState({
            price: '',
            yearly_gross: '',
            utilities: '',
            downpayment: '',
            rate: '',
            years: ''
        });

        event.preventDefault();
    }

    render(){
        return(
            <div className="container">
                <div>
                    <h2>Enter Values for a Property</h2>
                </div>
                <div>
                    <form id="property-value-form" onSubmit={this.propertyValue}>
                        <input type="number" name="price" className="form-control" placeholder="Price"
                        value={this.state.price} onChange={this.handleChange} required/>

                        <input type="number" name="yearly_gross" className="form-control" placeholder="Yearly Gross"
                        value={this.state.yearly_gross} onChange={this.handleChange} required/>    

                        <input type="number" name="downpayment" className="form-control" placeholder="Downpayment"
                        value={this.state.downpayment} onChange={this.handleChange} required/>

                        <input type="number" name="rate" className="form-control" placeholder="Mortgage Rate"
                        value={this.state.rate} onChange={this.handleChange} required/>

                        <input type="number" name="years" className="form-control" placeholder="Amortization Period"
                        value={this.state.years} onChange={this.handleChange} required/>

                        <select value={this.state.utilities} onChange={this.handleChange}
                        name="utilities" className="form-control" required >
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>
                        
            
                        <input className="btn btn-primary" type="submit" value="Enter"/>
                    </form>
                </div>
            </div>
        )
    }
}

export default Home;