from building import Building
from calculations import Calculations

building = Building(64900, 19200, "True")
# print(building.price_income_ratio())
building.set_tax(876)
building.set_insurance(60)


calculations = Calculations(20, 4.611, 30, building)
print("Down:    " + str(calculations.downpayment))
print("NOI:   " + str(calculations.noi()))
print("Cashflow:    " + str(calculations.cashflow()))
print("monthly payment:   " + str(calculations.monthly_payment()))
print("One percent without fees:   " + str(calculations.one_percent()))