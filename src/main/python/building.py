class Building:

        def __init__(self, price, yearly_gross, utilities):
            self.price = price
            self.yearly_gross = yearly_gross
            self.utilities = utilities
            self.tax = 0
            self.units = 0
            self.insurance = 0    

        def monthly_gross(self):
            return round(self.yearly_gross/12,2)

        def set_tax(self, tax):
            self.tax = tax   

        def set_insurance(self, insurance):
            self.insurance = insurance * 12

        def set_units(self, units):
            self.units = units

    