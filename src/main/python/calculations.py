class Calculations:

    def __init__(self, downpayment, rate, years, building):
        self.downpayment = downpayment * building.price/100
        self.rate = rate/100
        self.years = years
        self.building = building

    def monthly_payment(self):
        p = self.building.price - self.downpayment
        n = self.years * 12
        r = self.rate/12
        return round(p * (r * (1 + r) ** n / ((1 + r) ** n - 1)), 2)

    def noi(self):
        buidling = self.building
        income = buidling.yearly_gross
        
        return round(income - buidling.tax - buidling.insurance, 2)      
    
    def cashflow(self):
        buidling = self.building
        income = buidling.yearly_gross
        
        return round(income - buidling.tax - buidling.insurance - (self.monthly_payment() * 12), 2)        

    def one_percent(self):
        return round((self.building.monthly_gross())  / self.monthly_payment(),2)

    

    