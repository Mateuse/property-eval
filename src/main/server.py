from flask import Flask, request
from flask_cors import CORS, cross_origin
from python.building import Building   
from python.calculations import Calculations
import json

app = Flask(__name__)
cors = CORS(app)

@app.route("/")
def main():
    return "Property Evaluation \nUnder Construction"

@app.route("/api/building", methods=["POST"])
def building():
    return "building page"

@app.route("/api/calculations/all", methods=["POST"])
def calculations():
    data = json.loads(request.data.decode())

    building = Building(data['price'], data['yearly_gross'], data['utilities'])
    calculations = Calculations(data['downpayment'], data['rate'], data['years'], building)

    downpayment = calculations.downpayment
    cashflow = calculations.cashflow()
    noi = calculations.noi()
    monthly = calculations.monthly_payment()
    one = calculations.one_percent()

    response = {
        "downpayment": downpayment,
        "cashflow": cashflow,
        "noi": noi,
        "monthly": monthly,
        "one": one
    }

    return json.dumps(response)

@app.route("/test")
def test():
    return "This is a test page"

if __name__ == '__main__':
        app.run(host="0.0.0.0", port=3001)

# Sample post
# {
# "price": 100000,
# "yearly_gross": 10000,
# "utilities": true,
# "downpayment": 20,
# "rate": 5,
# "years": 30
# }