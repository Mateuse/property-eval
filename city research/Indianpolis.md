# Indianapolis

### Attributes
1. Population 863002
2. Capital of Indiana
3. 25 largest economic region in US

### People
1. Median Income household ---
2. Median Income family ---

### Economy
1. GDP $134 billion
2. Exports Pharmaceuticals, Motor Vehicle Parts, Medical Equipment, engine/ power equipment, aircraft products/parts
3. 2.8% Unemployment

### Industries
1. Finance, Insurance, Real Estate, Rental and Leasing ($30.7B)
2. Manufacturing ($30.1B)
3. Professional Business Services ($14.3B)
4. Education Services, Health Care, Social Assistance ($10.8B)
5. Wholesale Trade ($8.1B)

### Positive aspects
1. Ranks among fastest high-tech job growth areas in the US

#### Attractions
1. Indianapolis 500
2. Pacers (NBA) Colts (NFL)
3. Worlds largest Childrens museum
4. One of nations largest privately funded zoos

#### Demographics

| Race | 2016 |
| ---- | ---- |
| White | 61.6% |
| Black | 28.0% |
| Hispanic | 9.9% |
| Asian | 2.8% |
