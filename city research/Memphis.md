# Memphis

### Attributes
1. Population 652,236
2. 22nd most expansive city us (wide variety of landscapes and distinct neighborhoods)
3. Experiencing depopulation
4. 18.33/1000 resident crime rate [neighborhood scout](https://www.neighborhoodscout.com/tn/memphis/crime)
5. 3rd most dangerous city in US ** Crime rate has been lowring since 2006
6. Poorest large metro area 2011
7. Beale Street national historic landmark for american blues

### People
1. Median Income household $32,285
2. Median Income family $37,767

### Economy
1. Commerce in transportation and shipping industry
2. Increase in enterainment and film industry

### Industries
1. Transportation and Logistics (Fedex largest employer)

### Positive aspects
1. US leading commercial center in transportation and logistics
2. Second busiest cargo airport in the world

#### Attractions
1. National Civil Rights Museum
2. World Championship Barbecue Cooking Contest (100,000 visitors per annum)
